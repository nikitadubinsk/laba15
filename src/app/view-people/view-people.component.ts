import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { People } from '../app.component';

@Component({
  selector: 'app-view-people',
  templateUrl: './view-people.component.html',
  styleUrls: ['./view-people.component.css']
})
export class ViewPeopleComponent implements OnInit {

  @Input() people : People;
  @Output() onDelete: EventEmitter<People> = new EventEmitter<People>();

  constructor() { }

  deletePeople(id) {
    console.log("AAAAA");
    this.onDelete.emit(this.people);
  }

  ngOnInit() {
  }

}
