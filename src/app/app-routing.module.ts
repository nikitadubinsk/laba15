import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SeePeopleComponent } from './see-people/see-people.component';
import { CreatePeopleComponent } from './create-people/create-people.component';
import { EditPeopleComponent } from './edit-people/edit-people.component';


const routes: Routes = [
  {path: '', component: SeePeopleComponent},
  {path: 'create', component: CreatePeopleComponent},
  {path: 'edit/:id', component: EditPeopleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
