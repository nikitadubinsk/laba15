import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SeePeopleComponent } from './see-people/see-people.component';
import { EditPeopleComponent } from './edit-people/edit-people.component';
import { CreatePeopleComponent } from './create-people/create-people.component';
import { ViewPeopleComponent } from './view-people/view-people.component';
import { FilterPipe } from './see-people/pipes/filter.pipe';
import { HttpClientModule } from '@angular/common/http';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [
    AppComponent,
    SeePeopleComponent,
    EditPeopleComponent,
    CreatePeopleComponent,
    ViewPeopleComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    TextMaskModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
